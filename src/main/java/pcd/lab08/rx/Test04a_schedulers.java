package pcd.lab08.rx;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.*;
import io.reactivex.schedulers.*;

public class Test04a_schedulers {

	public static void main(String[] args) throws Exception {
		
		/* running on th main thread */
		
		log("RUNNING on main thread: ");
		doTest(Observable.just("a","b","c"));
		
		/* running on an executor based scheduler */
		
		int nThreads = Runtime.getRuntime().availableProcessors();
		ExecutorService exec = Executors.newFixedThreadPool(nThreads);
		Scheduler sched = Schedulers.from(exec);

		log("RUNNING on sched: ");

		doTest(Observable.just("a","b","c")
				.subscribeOn(sched)
				.doFinally(exec::shutdown));

		doTest(Observable.just("x","y","z")
				.subscribeOn(sched)
				.doFinally(exec::shutdown));

		log("DONE.");

	}
	
	private static void doTest(Observable<String> obs) {
		obs.map((v) -> {
			Thread.sleep(100);
			log("map1: "+v);
			return v+v;
		})
		.map((v) -> {
			Thread.sleep(100);
			log("map2: "+v);
			return v.length();
		})
		.subscribe((v) -> { 
			Thread.sleep(100);
			log("sub: "+v);
		});
	}

	static private void log(String msg) {
		System.out.println("[" + Thread.currentThread() + "] " + msg);
	}
	
}
