package pcd.lab08.rx;

import io.reactivex.*;

public class Test01_hello {

	public static void main(String[] args){
				
	    Flowable.just("Hello world").subscribe(s -> {	    		
	    		System.out.println(s);
	    });
	    
	    // Observable.just("Hello world").subscribe(System.out::println);
	    
		System.out.println("Simple subscription to a sync source...");

		// synch data source
		
		String[] words = { "Hello", " ", "World", "!\n" }; 
		
		// simple subscription 
		
		Flowable
		.fromArray(words)
		.subscribe((String s) -> {
			System.out.print(s + "-");
		});
		
		// full subscription: onNext(), onError(), onCompleted()

		System.out.println("Full subscription...");
		
		Observable
		.fromArray(words)
		.subscribe((String s) -> {
			System.out.println("> " + s);
			/*
			try { 
				Thread.sleep(1000); 
			} catch (Exception ex){}
			*/
		},(Throwable t) -> {
			System.out.println("error  " + t);
	    },() -> {
	    		System.out.println("completed");
	    });

        System.out.println("here.");
	
	}
	
	private static void whoAmI() {
		System.out.println("TH:"+Thread.currentThread().getName());
	}
}
