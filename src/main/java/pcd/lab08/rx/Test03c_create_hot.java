package pcd.lab08.rx;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.flowables.ConnectableFlowable;
import io.reactivex.schedulers.Schedulers;

public class Test03c_create_hot {

	public static void main(String[] args) throws Exception {

		log("creating the source.");
		Flowable<Integer> source = Flowable.create(emitter -> {		     
			new Thread(() -> {
				int i = 0;
				while (i < 100){
					try {
						log("source: "+i); 
						emitter.onNext(i);
						Thread.sleep(1000);
						i++;
					} catch (Exception ex){}
				}
			}).start();
		     //emitter.setCancellable(c::close);
		 }, BackpressureStrategy.BUFFER);

		Thread.sleep(2000);

		log("Making an hot observable.");
		
		ConnectableFlowable<Integer> hotObservable = source.publish();
		hotObservable.connect();
	
		Thread.sleep(5000);
		log("Subscribing A.");
		
		hotObservable.subscribe((s) -> {
			log("subscriber A: "+s); 
		});	
		
		Thread.sleep(5000);
		log("Subscribing B.");
		
		hotObservable.subscribe((s) -> {
			log("subscriber B: "+s); 
		});	
		
		log("Done.");

	}
	
	static private void log(String msg) {
		System.out.println("[" + Thread.currentThread() + "] " + msg);
	}
	

}
