package pcd.lab03.sync;

import java.util.LinkedList;
import java.util.List;

public class Counter {

	private int cont;
	
	public Counter(int base){
		this.cont = base;
	}
	
	public void inc(){
		cont++;
	}
		
	
	public int getValue(){
		return cont;
	}
	
}
