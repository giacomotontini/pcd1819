package pcd.lab05.monitors.cyclic_barrier;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BarrierImpl implements Barrier {

    private int nParticipants;
    private int actualParticipants;
    private Lock mutex;
    private Condition cond;
    
    public BarrierImpl(int nParticipants) {
            this.nParticipants = nParticipants;
            this.mutex = new ReentrantLock();
            this.cond = mutex.newCondition();
            actualParticipants = 0;
    }
    
    @Override
    public void hitAndWaitAll() throws InterruptedException {
            mutex.lock();
            
            actualParticipants++;
            if(actualParticipants < nParticipants) {
                cond.await();
                actualParticipants--;
            } else {
              cond.signalAll();
              actualParticipants--;
            } 

            mutex.unlock(); 
    }    
}