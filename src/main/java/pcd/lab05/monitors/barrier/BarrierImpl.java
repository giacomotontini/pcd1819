package pcd.lab05.monitors.barrier;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BarrierImpl implements Barrier {

    private int nParticipants;
    private Lock mutex;
    private Condition cond;
    
    public BarrierImpl(int nParticipants) {
            this.nParticipants = nParticipants;
            this.mutex = new ReentrantLock();
            this.cond = mutex.newCondition();
    }
    
    @Override
    public void hitAndWaitAll() throws InterruptedException {
            mutex.lock();
            nParticipants--;
            if(nParticipants > 0) {
                cond.await();
            } else {
              cond.signalAll();
            }
            mutex.unlock(); 
    }

    
}