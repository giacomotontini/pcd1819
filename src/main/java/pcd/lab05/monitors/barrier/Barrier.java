package pcd.lab05.monitors.barrier;

public interface Barrier {

	void hitAndWaitAll() throws InterruptedException;

}
