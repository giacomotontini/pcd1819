package pcd.lab05.monitors.barrier.sol;

public interface Barrier {

	void hitAndWaitAll() throws InterruptedException;

}
