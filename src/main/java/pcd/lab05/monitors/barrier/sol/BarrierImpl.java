package pcd.lab05.monitors.barrier.sol;

public class BarrierImpl implements Barrier {

	private int nHits;
	private int nParticipants;
	
	public BarrierImpl(int nParticipants) {
		this.nParticipants = nParticipants;
		nHits = 0;
	}

	@Override
	public synchronized void hitAndWaitAll() throws InterruptedException {
		nHits++;
		while (nHits < nParticipants) {
			wait();
		}
		notifyAll();
	}
}
