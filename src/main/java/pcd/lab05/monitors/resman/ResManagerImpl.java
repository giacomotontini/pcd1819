package pcd.lab05.monitors.resman;

import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ResManagerImpl implements ResManager {
    final Stack<Integer> res;
    
    public ResManagerImpl(int nResourcesAvailable) {
        res = new Stack<>();
        res.addAll(IntStream.range(0, nResourcesAvailable).boxed().collect(Collectors.toSet()));
        
    }
    
    @Override
    public synchronized int get() throws InterruptedException {
        while (res.size() == 0) {
            wait();
        }
        int resource = res.pop();
        return resource;
    }

    @Override
    public synchronized void release(int resource) {
        res.push(resource);
        notifyAll();
    }
    
}
