package pcd.lab06.executors.quad3_withfuture_mysol;

public interface IFunction {

	public double eval(double val);
	
}
