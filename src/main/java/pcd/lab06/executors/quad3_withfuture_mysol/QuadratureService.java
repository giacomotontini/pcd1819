package pcd.lab06.executors.quad3_withfuture_mysol;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;

public class QuadratureService {
    
    private int numTasks;
    private ExecutorService executor;
    
    public QuadratureService(int numTasks, int poolSize) {
        this.numTasks = numTasks;
        executor = Executors.newFixedThreadPool(poolSize);
    }
    
    public double compute(IFunction mf, double a, double b) throws InterruptedException {
        
        double x0 = a;
        double step = (b - a) / numTasks;
        
        Set<Future<Double>> results = new HashSet<Future<Double>>();
        for (int i = 0; i < numTasks; i++) {
            final Callable<Double> callable = new ComputeAreaTask(x0, x0 += step, mf);
            final Future<Double> future = executor.submit(callable);
            log("Submitted task " + x0 + " " + (x0 + step));
            results.add(future);
        }

        return results.stream().map(r -> {
            try {
                return r.get();
            } catch (InterruptedException | ExecutionException e) {
                System.err.println("An error occurred while retriving a result");
            }
            return 0.0;
        }).reduce(Double::sum).get();
        
    }
    
    private void log(String msg) {
        System.out.println("[SERVICE] " + msg);
    }
}
