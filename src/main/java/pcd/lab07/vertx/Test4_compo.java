package pcd.lab07.vertx;

import io.vertx.core.AsyncResult;
import io.vertx.core.*;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;

public class Test4_compo {

	public static void main(String[] args) {
		Vertx  vertx = Vertx.vertx();
		FileSystem fs = vertx.fileSystem();    		
		Future<Buffer> f1 = Future.future();
		Future<Buffer> f2 = Future.future();
		
		fs.readFile("build.gradle", f1);
		fs.readFile("settings.gradle", f2);
		
		CompositeFuture.all(f1,f2).setHandler((AsyncResult<CompositeFuture> res) -> {
			log("COMPOSITE => \n"+res.result().list());			
		});
	}

	private static void log(String msg) {
		System.out.println("" + Thread.currentThread() + " " + msg);
	}
}

