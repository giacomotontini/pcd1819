package pcd.lab07.vertx;
import io.vertx.core.*;


public class Test3_future {
	public static void main(String[] args) {
		
		Vertx vertx = Vertx.vertx();

		log("pre");
		
		Future<Void> fut = Future.future();
		fut.setHandler(ar -> {
			log("done");
		});
		
		vertx.setTimer(1000, res -> {
			log("timeout");
			fut.complete();
		});
		/*
		try {
			Thread.sleep(2000);
		} catch (Exception ex) {}
		*/
		log("post");
	}
		
	private static void log(String msg) {
		System.out.println("" + Thread.currentThread() + " " + msg);
	}
}

